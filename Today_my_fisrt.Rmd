---
title: "myfirst"
author: "Paulo"
date: "2024-05-30"
output: html_document
---

# Introduction

This is a report to understand red wine quality.

Step1. Load libraries and data

```{r load library and data}

library(reshape2)

library(ggplot2)

df <- read.csv("RMark/winequality-red.csv") 
str(df)
library(readr)
winequality_red <- read_csv("C:/Users/paulo/Desktop/RMark/winequality-red.csv")
str(df)



```

## Step2. Preprosses data

We want to know which columns affect the quality of red wine
r preprocess data and visualize

```{r}
df_long = melt(df, id.vars="quality")

ggplot(df_long, aes(x = quality, y = value)) +
  geom_point(alpha = 0.5) + 
  facet_wrap(~variable, 
  scales = "free_y") +
  labs(title = "Scatter Plots of Each Variable Against Quality",
    x = "quality",
    y = "Value") +
theme_bw()
```

## Step3. use a regression model

```{r}
model = lm(quality ~., data=df)
summary(model)
```

## 

```         
```

[![](images/Redwinepicture.jpg){width="530"}](https://zerowasteeurope.eu/library/the-story-of-rewine/)

{r cars} summary(cars)

\`\`\`{r setup, include=FALSE} knitr::opts_chunk\$set(echo = TRUE)

This is an R Markdown document. Markdown is a simple formatting syntax for authoring HTML, PDF, and MS Word documents. For more details on using R Markdown see <http://rmarkdown.rstudio.com>.

When you click the **Knit** button a document will be generated that includes both content as well as the output of any embedded R code chunks within the document. You can embed an R code chunk like this:

```{r cars}
summary(cars)
```

## Including Plots

You can also embed plots, for example:

```{r pressure, echo=FALSE}
plot(pressure)
```

Note that the `echo = FALSE` parameter was added to the code chunk to prevent printing of the R code that generated the plot.
